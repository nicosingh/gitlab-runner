---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# Autoscaling GitLab Runner on GCP GCE

One of the biggest advantages of GitLab Runner is its ability to automatically
spin up and down VMs to make sure your builds get processed immediately. It's a
great feature, and if used correctly, it can be extremely useful in situations
where you don't use your Runners 24/7 and want to have a cost-effective and
scalable solution.

## Introduction

In this tutorial, we'll explore how to properly configure a GitLab Runner in GCP
that will serve as the Runner Manager where it will spawn new Docker machines on
demand.

In addition, we'll make use of [Google Cloud's GCE Preemptible VM instances](https://cloud.google.com/compute/docs/instances/preemptible)
which will greatly reduce the costs of the Runner instances while still using
quite powerful autoscaling machines.

## Prerequisites

NOTE: **Note:**
A familiarity with Google Compute Cloud (GCP) is required as this is where most
of the configuration will take place.

TIP: **Tip:**
We suggest a quick read through Docker Machine [`google` driver documentation](https://docs.docker.com/machine/drivers/gce/)
to familiarize yourself with the parameters we will set later in this article.

Your GitLab instance is going to need to talk to the Runners over the network,
and that is something you need think about when configuring any Network tags or
when setting up your DNS configuration.

For example, you can keep the GCE resources segmented away from public traffic
in a different VPC to better strengthen your network security. Your environment
is likely different, so consider what works best for your situation.

### GCP credentials

As mentioned in [`google` driver documentation](https://docs.docker.com/machine/drivers/gce/#credentials)
You'll need to configure Google Application Credentials tied to a service
account with permission to scale (GCE) and update the cache (via GCS).
This service account must have IAM `compute.instances.*` and `storage.objects.*`
permissions to scale or use the cache respectively. These Credentials can be
either assigned to the Runner Manager server at moment of creation, or using
`GOOGLE_APPLICATION_CREDENTIALS` environment variable.

## Prepare the Runner Manager instance

The first step is to install GitLab Runner in a GCE instance that will serve
as the Runner Manager that spawns new machines. This doesn't have to be a
powerful machine since it will not run any jobs itself, a `e2-micro` instance
will do. This machine will be a dedicated host since we need it always up and
running, thus it will be the only standard cost.

NOTE: **Note:**
For the Runner Manager instance, choose a distribution that both Docker and
GitLab Runner support, for example either Ubuntu, Debian, CentOS or RHEL will
work fine.

Install the prerequisites:

1. Log in to your server
1. [Install GitLab Runner from the official GitLab repository](../../install/linux-repository.md)
1. [Install Docker](https://docs.docker.com/engine/installation/#server)
1. [Install Docker Machine](https://docs.docker.com/machine/install-machine/)

Now that the Runner is installed, it's time to register it.

## Registering the GitLab Runner

Before configuring the GitLab Runner, you need to first register it, so that
it connects with your GitLab instance:

1. [Obtain a Runner token](https://docs.gitlab.com/ee/ci/runners/)
1. [Register the Runner](../../register/index.md#linux)
1. When asked the executor type, enter `docker+machine`

You can now move on to the most important part, configuring the GitLab Runner.

TIP: **Tip:**
If you want every user in your instance to be able to use the autoscaled Runners,
register the Runner as a shared one.

## Configuring the GitLab Runner

Now that the Runner is registered, you need to edit its configuration file and
add the required options for the Google machine driver.

Let's first break it down to pieces.

### The global section

In the global section, you can define the limit of the jobs that can be run
concurrently across all Runners (`concurrent`). This heavily depends on your
needs, like how many users your Runners will accommodate, how much time your
builds take, etc. You can start with something low like `10`, and increase or
decrease its value going forward.

The `check_interval` option defines how often the Runner should check GitLab
for new jobs, in seconds.

Example:

```toml
concurrent = 10
check_interval = 0
```

[Other options](../advanced-configuration.md#the-global-section)
are also available.

### The `runners` section

From the `[[runners]]` section, the most important part is the `executor` which
must be set to `docker+machine`. Most of those settings are taken care of when
you register the Runner for the first time.

`limit` sets the maximum number of machines (running and idle) that this Runner
will spawn. For more information, check the [relationship between `limit`,
`concurrent` and `IdleCount`](../autoscale.md#how-concurrent-limit-and-idlecount-generate-the-upper-limit-of-running-machines).

Example:

```toml
[[runners]]
  name = "gitlab-gcp-autoscaler"
  url = "<URL of your GitLab instance>"
  token = "<Runner's token>"
  executor = "docker+machine"
  limit = 20
```

[Other options](../advanced-configuration.md#the-runners-section)
under `[[runners]]` are also available.

### The `runners.docker` section

In the `[runners.docker]` section you can define the default Docker image to
be used by the child Runners if it's not defined in [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/).
By using `privileged = true`, all Runners will be able to run
[Docker in Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor)
which is useful if you plan to build your own Docker images via GitLab CI/CD.

Next, we use `disable_cache = true` to disable the Docker executor's inner
cache mechanism since we will use the distributed cache mode as described
in the following section.

Example:

```toml
  [runners.docker]
    image = "alpine"
    privileged = true
    disable_cache = true
```

[Other options](../advanced-configuration.md#the-runnersdocker-section)
under `[runners.docker]` are also available.

### The `runners.cache` section

To speed up your jobs, GitLab Runner provides a cache mechanism where selected
directories and/or files are saved and shared between subsequent jobs.
While not required for this setup, it is recommended to use the distributed cache
mechanism that GitLab Runner provides. Since new instances will be created on
demand, it is essential to have a common place where the cache is stored.

In the following example, we use Google Cloud Storage:

```toml
  [runners.cache]
    Type = "gcs"
    Shared = true
    [runners.cache.gcs]
      BucketName = "runners-cache"
```

Here's some more information to further explore the cache mechanism:

- [Reference for `runners.cache`](../advanced-configuration.md#the-runnerscache-section)
- [Reference for `runners.cache.gcs`](../advanced-configuration.md#the-runnerscachegcs-section)
- [Deploying and using a cache server for GitLab Runner](../autoscale.md#distributed-runners-caching)
- [How cache works](https://docs.gitlab.com/ee/ci/yaml/#cache)

### The `runners.machine` section

This is the most important part of the configuration and it's the one that
tells GitLab Runner how and when to spawn new or remove old Docker Machine
instances.

We will focus on the GCP machine options, for the rest of the settings read
about the:

- [Autoscaling algorithm and the parameters it's based on](../autoscale.md#autoscaling-algorithm-and-parameters) - depends on the needs of your organization
- [Autoscaling periods](../autoscale.md#autoscaling-periods-configuration) - useful when there are regular time periods in your organization when no work is done, for example weekends

Here's an example of the `runners.machine` section:

```toml
  [runners.machine]
    IdleCount = 1
    IdleTime = 1800
    MaxBuilds = 10
    MachineDriver = "google"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = [
      "google-project=xxxxx",
      "google-network=xxxxx",
      "google-subnetwork=xxxxx",
      "google-use-internal-ip=true",
      "google-machine-type=e2-standard-4",
      "google-disk-size=10",
      "google-disk-type=pd-standard",
      "google-machine-image=https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts",
      "google-username=root",
    ]
    [[runners.machine.autoscaling]]
      Periods = ["* * 9-17 * * mon-fri *"]
      IdleCount = 50
      IdleTime = 3600
      Timezone = "UTC"
    [[runners.machine.autoscaling]]
      Periods = ["* * * * * sat,sun *"]
      IdleCount = 5
      IdleTime = 60
      Timezone = "UTC"
```

The Docker Machine driver is set to `google` and the machine name has a standard
prefix followed by `%s` (required) that is replaced by the ID of the child
Runner: `gitlab-docker-machine-%s`.

Now, depending on your GCP infrastructure, there are many options you can set up
under `MachineOptions`. Below you can see the most common ones.

| Machine option | Description |
| -------------- | ----------- |
| `google-project=xxxxx` | **required** The ID of your project to use when launching the instance. |
| `google-network=xxxxx` | Network in which to provision VM. |
| `google-subnetwork=xxxxx` | Subnetwork in which to provision VM. |
| `google-use-internal-ip=true` | Use internal IP address to communicate with autoscaled VMs. |
| `google-machine-type=e2-standard-4` | The type of instance. See [machine types](https://cloud.google.com/compute/docs/machine-types) documentation. |
| `google-disk-size=10` | The disk size of instance. |
| `google-disk-type=pd-standard` | The disk type of instance. |
| `google-machine-image=https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts` | The absolute URL to a base VM image to instantiate. It also supports [image families](https://docs.docker.com/machine/drivers/gce/#options). |
| `google-username=root` | The username to be used in the VM. |

TIP: **Tip:**
Under `MachineOptions` you can add anything that the [GCP Docker Machine driver
supports](https://docs.docker.com/machine/drivers/gce/#options). You are highly
encouraged to read Docker's docs as your infrastructure setup may warrant
different options to be applied.

NOTE: **Note:**
The child instances will use by default Ubuntu 16.04 unless you choose a
different image ID/Family by setting `google-machine-image`. Set only [supported
base operating systems for Docker Machine](https://docs.docker.com/machine/drivers/os-base/).

NOTE: **Note:**
If you specify `google-use-internal-ip-only=true` as one of the machine options,
the new VM will not have a public IP address assigned and will not have Internet
access, which means we will have to prepare a Cloud NAT or manual routing setup
if our runner VMs require internet connectivity. This option is useful only when
the host running `docker-machine` is located inside the Google Cloud
infrastructure; otherwise, `docker-machine` will not reach the VM to provision
the Docker daemon. The presence of this flag implies `--google-use-internal-ip`.

[Other options](../advanced-configuration.md#the-runnersmachine-section)
under `[runners.machine]` are also available.

### Getting it all together

Here's the full example of `/etc/gitlab-runner/config.toml`:

```toml
concurrent = 10
check_interval = 0

[[runners]]
  name = "gitlab-gcp-autoscaler"
  url = "<URL of your GitLab instance>"
  token = "<Runner's token>"
  executor = "docker+machine"
  limit = 20
  [runners.docker]
    image = "alpine"
    privileged = true
    disable_cache = true
  [runners.cache]
    Type = "gcs"
    Shared = true
    [runners.cache.gcs]
      BucketName = "runners-cache"
  [runners.machine]
    IdleCount = 1
    IdleTime = 1800
    MaxBuilds = 10
    MachineDriver = "google"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = [
      "google-project=xxxxx",
      "google-network=xxxxx",
      "google-subnetwork=xxxxx",
      "google-use-internal-ip=true",
      "google-machine-type=e2-standard-4",
      "google-disk-size=10",
      "google-disk-type=pd-standard",
      "google-machine-image=https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts",
      "google-username=root",
    ]
    [[runners.machine.autoscaling]]
      Periods = ["* * 9-17 * * mon-fri *"]
      IdleCount = 50
      IdleTime = 3600
      Timezone = "UTC"
    [[runners.machine.autoscaling]]
      Periods = ["* * * * * sat,sun *"]
      IdleCount = 5
      IdleTime = 60
      Timezone = "UTC"
```

## Cutting down costs with Google Cloud Preemptible VM instances

As [described by](https://cloud.google.com/compute/docs/instances/preemptible) Google:

>
A preemptible VM is an instance that you can create and run at a much lower
price than normal instances. However, Compute Engine might stop (preempt) these
instances if it requires access to those resources for other tasks. Preemptible
instances are excess Compute Engine capacity, so their availability varies with
usage.

In addition to the [`runners.machine`](#the-runnersmachine-section) options
you picked above, in `/etc/gitlab-runner/config.toml` under the `MachineOptions`
section, add the following:

```toml
    MachineOptions = [
      "google-preemptible=true",
    ]
```

### Caveats of Preemptible VM instances

While Preemptible VM instances is a great way to use unused resources and
significantly minimize the costs of your infrastructure, you must be aware of
the implications.

Running CI jobs on Preemptible VM instances may increase the failure rates
because of possible instance stop (preemption). Google Compute Engine will stop
these instances if resources are required for other tasks, or after they run for
24 hours. For more details about Limitations and the preemption Process, please
refer to the official documentation about [Preemptible VM instances](https://cloud.google.com/compute/docs/instances/preemptible)
.

## Conclusion

In this guide we learned how to install and configure a GitLab Runner in
autoscale mode on GCP.

Using the autoscale feature of GitLab Runner can save you both time and money.
Using the Preemptible VM instances that GCP provides can save you even more, but
you must be aware of the implications.
